<?php

/**
 * @file
 * osCaddie Alfresco module provides the basic API calls to Alfresco using a JSON
 * REST API and global utility functions.
 */

/**
 * RESOURCES PATHS.
 */
define('OSCADDIE_ALFRESCO_RESOURCE_PATH_LOGIN', 'api/login.json');
define('OSCADDIE_ALFRESCO_RESOURCE_PATH_CONTENT', 'canopy/content.json');
define('OSCADDIE_ALFRESCO_RESOURCE_PATH_FILE', 'canopy/file.json');
define('OSCADDIE_ALFRESCO_RESOURCE_PATH_REGISTER', 'canopy/register.json');
define('OSCADDIE_ALFRESCO_RESOURCE_PATH_REGISTER_TYPE', 'canopy/regtype.json');
define('OSCADDIE_ALFRESCO_RESOURCE_PATH_TYPE', 'canopy/types.json');
define('OSCADDIE_ALFRESCO_RESOURCE_PATH_TYPE_MAP', 'canopy/maptype.json');
define('OSCADDIE_ALFRESCO_CONTENT_TYPE_DELETE_TYPE', 'canopy/removetype');
define('OSCADDIE_ALFRESCO_NODE_ATTACH_FILE', 'canopy/file');

/**
 * GENERAL STATUS & ERROR CODE.
 */
define('OSCADDIE_ALFRESCO_CODE_INVALID', 10);
define('OSCADDIE_ALFRESCO_CODE_ALFRESCO_NOT_RESPONDING', 11);
define('OSCADDIE_ALFRESCO_CONNECTION_OK', 200);
define('OSCADDIE_ALFRESCO_CONNECTION_CREATED', 201);
define('OSCADDIE_ALFRESCO_CONNECTION_ACCEPTED', 202);
define('OSCADDIE_ALFRESCO_CONNECTION_UNAUTHORIZED', 401);
define('OSCADDIE_ALFRESCO_CONNECTION_FORBIDDEN', 403);
define('OSCADDIE_ALFRESCO_CONNECTION_NOT_FOUND', 404);
define('THIRTY_MINUTES', 1800);

/**
 * Implements hook_menu().
 */
function oscaddie_alfresco_menu() {
  //$items['oscaddie_alfresco/ajax/alfresco-status'] = array(
  //  'title' => 'Alfresco Server Status',
  //  'page callback' => 'oscaddie_alfresco__ajax_alfresco_status',
  //  'access arguments' => array('access content'),
  //  'type' => 'MENU_CALLBACK',
  //);

  //
  // ADMIN MENU ITEMS.
  //
  $items['admin/oscaddie_alfresco'] = array(
    'title' => "osCaddie Alfresco",
    'page callback' => 'oscaddie_alfresco__admin__dashboard',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/oscaddie_alfresco.admin.inc',
    'menu_name' => 'management',
  );

  // Portal Configuration.
  $items['admin/oscaddie_alfresco/portal'] = array(
    'title' => "Portal Configuration",
    'page callback' => 'oscaddie_alfresco__admin__portal',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/oscaddie_alfresco.admin.inc',
    'type' => MENU_NORMAL_ITEM,
    'weight' => 10,
    'menu_name' => 'management',
  );

  $items['admin/oscaddie_alfresco/portal/alfresco/add'] = array(
    'title' => "Add Alfresco server",
    'page callback' => 'oscaddie_alfresco__admin__portal_afresco',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/oscaddie_alfresco.admin.inc',
    'type' => MENU_CALLBACK,
    'weight' => 11,
    'menu_name' => 'management',
  );

  $items['admin/oscaddie_alfresco/portal/alfresco/edit'] = array(
    'title' => "Edit Alfresco server",
    'page callback' => 'oscaddie_alfresco__admin__portal_afresco',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/oscaddie_alfresco.admin.inc',
    'type' => MENU_CALLBACK,
    'weight' => 12,
    'menu_name' => 'management',
  );

  $items['admin/oscaddie_alfresco/portal/drupal/add'] = array(
    'title' => "Add Drupal site",
    'page callback' => 'oscaddie_alfresco__admin__portal_drupal',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/oscaddie_alfresco.admin.inc',
    'type' => MENU_CALLBACK,
    'weight' => 13,
    'menu_name' => 'management',
  );

  $items['admin/oscaddie_alfresco/portal/drupal/edit'] = array(
    'title' => "Edit Drupal site",
    'page callback' => 'oscaddie_alfresco__admin__portal_drupal',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/oscaddie_alfresco.admin.inc',
    'type' => MENU_CALLBACK,
    'weight' => 14,
    'menu_name' => 'management',
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function oscaddie_alfresco_theme($existing, $type, $theme, $path) {
  $themes['oscaddie_alfresco_admin'] = array(
    'template' => 'templates/admin.wrapper',
    'variables' => array('content' => NULL),
  );

  $themes['oscaddie_alfresco_admin_content'] = array(
    'template' => 'templates/admin.content',
    'variables' => array('sidebar' => NULL, 'content' => NULL),
  );

  $themes['oscaddie_alfresco_admin_sidebar'] = array(
    'template' => 'templates/admin.content.sidebar',
    'variables' => array('sections' => NULL),
  );

  $themes['oscaddie_alfresco_admin_content_dashboard'] = array(
    'template' => 'templates/admin.content.dashboard',
    'variables' => array('sections' => NULL),
  );

  $themes['oscaddie_alfresco_admin_content_portal'] = array(
    'template' => 'templates/admin.content.portal',
    'variables' => array('alfresco' => NULL, 'drupal' => NULL),
  );

  return $themes;
}


/**
 * Create a Request URI supporting query strings.
 *
 * @param string $resource
 *   Path to the resource. Do not include leading slashes.
 * @param array $query
 *   Optional array of query parameters to be included in the URI.
 *
 * @return string
 *   A fully qualified request URI.
 */
function oscaddie_alfresco__alfresco_api__request_create_uri($resource, $query = array()) {
  // TODO: Add error checking for invalid settings.
  $api_settings = variable_get('oscaddie_alfresco_settings_alfresco', array());

  if (empty($api_settings)) {
    return FALSE;
  }

  // TODO: Allow different protocols.
  $path = sprintf("http://%s:%s/%s/%s", $api_settings['address'], $api_settings['port'], $api_settings['endpoint'], $resource);
  $uri_options = array('external' => TRUE, 'alias' => TRUE, 'query' => $query);

  return url($path, $uri_options);
}

/**
 * Base method to send a request to Alfresco.
 *
 * @param string $path
 *   Resource path that the data will be sent.
 * @param mixed $data
 *   Data that is being sent and will be encoded in JSON automatically. (Optional)
 * @param string $method
 *   HTTP method for the request operation. Default to POST.
 * @param array $headers
 *   An array containing request headers. (Optional)
 * @param array $query
 *   An array containing additional query string parameters to be added to the
 *   request URL. (Optional)
 * @param bool $json_encode
 *   Encode the contents of the data into JSON. Useful if the content is not already in JSON.
 *
 * @return object
 *   A Response object of the request.
 */
function oscaddie_alfresco__alfresco_api__send($path, $data = NULL, $method = 'POST', $headers = array(), $query = array(), $json_encode = TRUE) {
  if (!($api_ticket = oscaddie_alfresco__alfresco_api__ticket_get(TRUE))) {
    return FALSE;
  }

  $content = NULL;
  $query += array('alf_ticket' => $api_ticket);
  $uri = oscaddie_alfresco__alfresco_api__request_create_uri($path, $query);

  if (!empty($data)) {
    // Just encode the data into JSON without any further processing.
    $content = $json_encode ? json_encode($data) : $data;
  }

  // Make a HTTP request.
  try {
    $options = array('headers' => $headers, 'method' => $method, 'data' => $content);
    $response = drupal_http_request($uri, $options);

    // TODO: Add configurable multiple retries.
    // TODO: Add error handling.
    return json_decode($response->data);
  }
  catch (Exception $e) {
    throw new Exception('Failed to get REST response', 0, $e);
  }
}

/**
 * Request an API ticket from an Alfresco server.
 *
 * @param bool $reset
 *   Set to TRUE to request a new API ticket from Alfresco.
 *
 * @return string
 *   A token to be used for all Alfresco API calls, otherwise NULL.
 */
function oscaddie_alfresco__alfresco_api__ticket_get($reset = FALSE) {
  $api_settings = variable_get('oscaddie_alfresco_settings_alfresco', array());

  // Get ticket from static cache if any.
  $session = (isset($_SESSION['oscaddie_alfresco_alfresco_api']) ? $_SESSION['oscaddie_alfresco_alfresco_api'] : '');

  if (!empty($session) && $reset == FALSE) {
    // Check if API username or password have changed.
    if ($session['username'] == $api_settings['username'] &&
      $session['password'] == $api_settings['password']) {
      // Check if ticket has not expired then return it.
      if (!empty($session['ticket']) && $session['expires'] >= time()) {
        return $session['ticket'];
      }
    }
  }

  $query = array('u' => $api_settings['username'], 'pw' => $api_settings['password']);
  $uri = oscaddie_alfresco__alfresco_api__request_create_uri(OSCADDIE_ALFRESCO_RESOURCE_PATH_LOGIN, $query);

  try {
    $response = drupal_http_request($uri);
    if (empty($response)) {
      // Server not responding.
      drupal_message(t("SERVER NOT RESPONDING"));
      watchdog('oscaddie_alfresco', 'Server not responding', array(), WATCHDOG_ERROR);
      return FALSE;
    }

    // TODO: DO error checking on request.
    if ($response->code == OSCADDIE_ALFRESCO_CONNECTION_OK) {
      $data = json_decode($response->data);
      $ticket = $data->data->ticket;

      $_SESSION['oscaddie_alfresco_alfresco_api'] = array(
        'ticket' => $ticket,
        'expires' => time() + THIRTY_MINUTES,
        'username' => $api_settings['username'],
        'password' => $api_settings['password'],
      );

      return $ticket;
    }
    elseif ($response->code == OSCADDIE_ALFRESCO_CONNECTION_FORBIDDEN) {
      // Wrong credentials.
      return FALSE;
    }

    return FALSE;
  }
  catch (Exception $e) {
    throw new Exception('Failed to get REST response', 0, $e);
  }
}

/**
 * Register the Drupal site into Alfresco.
 *
 * @param string $name
 *   Name that will be used in the registration.
 * @param string $description
 *   Additional information describing the site.
 *
 * @return bool
 *   TRUE if registration is successful otherwise FALSE.
 */
function oscaddie_alfresco__alfresco_api__site_register($name, $description = "") {
  if (empty($name)) {
    return FALSE;
  }

  // TODO: Need to double check on HTTP HOST.
  $data = (object) array(
    'canopy' => (object) array(
      'site_name' => $name,
      'site_description' => $description,
      'service_root' => sprintf("http://%s", $_SERVER['HTTP_HOST']),
    ),
  );

  try {
    $response = oscaddie_alfresco__alfresco_api__send(OSCADDIE_ALFRESCO_RESOURCE_PATH_REGISTER, $data);
// dpm($response);
    if ($response->response->status == 'success') {
      return TRUE;
    }
    elseif ($response->response->status == 'fail') {
      return FALSE;
    }
    else {
      // Server not responding.
      return FALSE;
    }
  }
  catch (Exception $e) {
    throw new Exception('Failed to get REST response', 0, $e);
  }
}

/**
 * API Method to create content in Alfresco.
 *
 * @param mixed $content
 *   Content that is being created in Alfresco.
 */
function oscaddie_alfresco__alfresco_api__content_create($content) {
  if (empty($content)) {
    return OSCADDIE_ALFRESCO_CODE_INVALID;
  }

  $response = oscaddie_alfresco__alfresco_api__send(OSCADDIE_ALFRESCO_RESOURCE_PATH_CONTENT, $content, 'POST');

  if (empty($response)) {
    drupal_set_message(t("Alfresco server is not responding. Please contact your network administrator for support."), 'error');
    watchdog('oscaddie_alfresco', 'Alfresco server is not responding.', array(), WATCHDOG_ERROR);
    return FALSE;
  }

  // TODO: Error handling.
  return $response->response;
}

/**
 * API Method to update content in Alfresco.
 *
 * @param mixed $content
 *   Content that is being updated in Alfresco.
 */
function oscaddie_alfresco__alfresco_api__content_update($content) {
  if (empty($content)) {
    return OSCADDIE_ALFRESCO_CODE_INVALID;
  }

  $response = oscaddie_alfresco__alfresco_api__send(OSCADDIE_ALFRESCO_RESOURCE_PATH_CONTENT, $content, 'PUT');

  return $response->response;
}

/**
 * TODO: API Method to delete content in Alfresco.
 */
function oscaddie_alfresco__alfresco_api__content_delete() {

}

/**
 * Retrieve all Content Models from Alfresco.
 *
 * All content models are retrieved regardless if they are mapped to any
 * Drupal Content-Type or not.
 *
 * @return array
 *   An array of Content Model objects, otherwise FALSE if error.
 */
function oscaddie_alfresco__alfresco_api__types_get() {
  $response = oscaddie_alfresco__alfresco_api__send(OSCADDIE_ALFRESCO_RESOURCE_PATH_TYPE, NULL, 'GET');

  if (!empty($response)) {
    if ($response->response->status == 'success') {
      return $response->response->array;
    }
    else {
      return FALSE;
    }

  }
  else {
    watchdog('oscaddie_alfresco', "Unable to retrieve Alfresco content models.", array(), WATCHDOG_ERROR);

    return FALSE;
  }
}

/**
 * Create a new Content Model on Alfresco.
 *
 * Content Model is based on a Drupal Content-Type definition.
 * Only non-existing Content Model will be created, existing ones
 * will be ignored.
 *
 * @param array $definitions
 *   An array of Drupal content-types
 */
function oscaddie_alfresco__alfresco_api__types_register($definitions) {
  if (empty($definitions) && !is_array($definitions)) {
    return OSCADDIE_ALFRESCO_CODE_INVALID;
  }

  $data = (object) array(
    'definition' => (object) array('array' => $definitions),
  );

  $response = oscaddie_alfresco__alfresco_api__send(OSCADDIE_ALFRESCO_RESOURCE_PATH_REGISTER_TYPE, $data);
  switch ($response->response->status) {
    case 'success':
      return (!empty($response->response->types) ? (array) $response->response->types : FALSE);

    case 'fail':
      return FALSE;

    default:
      return OSCADDIE_ALFRESCO_CODE_ALFRESCO_NOT_RESPONDING;
  }
}

/**
 * Map a Drupal content-type to an Alfresco along with all its fields.
 *
 * @param array $definitions
 *   An array of mapping definitions.
 *
 * @return bool
 *   TRUE if successful otherwise FALSE.
 */
function oscaddie_alfresco__alfresco_api__types_map($definitions) {
  if (empty($definitions) && !is_array($definitions)) {
    return FALSE;
  }

  $data = (object) array(
    'map' => (object) array('array' => $definitions),
  );

  $response = oscaddie_alfresco__alfresco_api__send(OSCADDIE_ALFRESCO_RESOURCE_PATH_TYPE_MAP, $data);

  if ($response->response->status == 'success') {
    return TRUE;
  }
  elseif ($response->response->status == 'fail') {
    return FALSE;
  }
  else {
    return FALSE;
  }
}

/**
 * Checks the status of Alfresco server instance.
 */
function oscaddie_alfresco__ajax_alfresco_status() {
  $alfresco_settings = variable_get('oscaddie_alfresco_settings_alfresco', array());

  // Checks to see if Alfresco settings have been done.
  if (empty($alfresco_settings)) {
    return FALSE;
  }

  try {
    // Bypass PHP warning error if Alfresco is not responding.
    $conn = @fsockopen($alfresco_settings['address'], $alfresco_settings['port'], $errno, $errstr, 5);

    if ($conn) {
      fclose($conn);
      drupal_json(TRUE);
    }
    else {
      drupal_json(FALSE);
    }
  }
  catch (Exception $e) {
    throw new Exception('Failed to open fsocket', 0, $e);
  }
}

/**
 * Package the Mapping Definition schema into an Alfresco acceptable format.
 * @param  array  $definitions An array containing the definitions of a Drupal
 *                             content-type mapped to an Alfresco type.
 * @return array               An array of converted definitions objects.
 *                             FALSE if error.
 */
function oscaddie_alfresco__alfresco_api__map_definition_package($definitions) {
  if (empty($definitions) || !is_array($definitions)) {
    return FALSE;
  }

  $package = array();

  foreach ($definitions as $definition) {
    $properties = array();

    foreach ($definition['fields'] as $idx => $field) {
      $property = new stdClass();
      $property->drupalName   = $field['field_name_drupal'];
      $property->alfrescoName = $field['field_name_alfresco'];

      $properties[] = $property;
    }

    $package[] = (object) array(
      'drupalName'   => $definition['type_drupal'],
      'alfrescoName' => $definition['type_alfresco'],
      'syncModel'    => !empty($definitions['sync_model']) ? $definitions['sync_model'] : "2",
      'creator'      => $definition['creator'],
      'props'        => $properties,
    );

    return $package;

  }

}

/**
 * Given a type, return the corresponding Alfresco data type.
 * @param  string $type Drupal type.
 * @return string       Alfresco data type.
 */
function oscaddie_alfresco__drupal_type_conversion($type) {
  $datatypes = array(
    'file'              => 'any',
    'list_boolean'      => 'boolean',
    'list_float'        => 'float',
    'list_integer'      => 'int',
    'list_text'         => 'text',
    'number_decimal'    => 'double',
    'number_float'      => 'float',
    'number_integer'    => 'int',
    'text'              => 'text',
    'text_long'         => 'text',
    'text_with_summary' => 'text',
  );
  return array_key_exists($type, $datatypes) ? $datatypes[$type] : FALSE;
}

/**
 * Enforces the conversion type set by osCaddie Alfresco Model.
 * @param  mixed  $value Value to be sent to Alfresco.
 * @param  string $type  Drupal data type to be converted.
 * @return mixed         Appropriate object type.
 */
function oscaddie_alfresco__enforce_types($value, $type) {
  $conversion = oscaddie_alfresco__drupal_type_conversion($type);
  switch ($conversion) {
  case 'boolean':
    return (bool)   $value;
  case 'float':
    return (float)  $value;
  case 'int':
    return (int)    $value;
  case 'text':
    return (string) $value;
  case 'double':
    return (double) $value;
  default:
    return FALSE;
}
}

/**
 * API Method to create file in Alfresco.
 *
 * @param mixed $content
 *   Content that is being created in Alfresco.
 */
function oscaddie_alfresco__alfresco_api__file_create($content) {
  if (empty($content)) {
    return OSCADDIE_ALFRESCO_CODE_INVALID;
  }
  $boundary = '--' . hash('sha256', uniqid(REQUEST_TIME));
  $data = "--{$boundary}\r\n";
  $data .= 'Content-Disposition: form-data; name="file"';
  $data .= "\r\nContent-Type: {$content->content->mime_type}\r\n\r\n";
  $data .= $content->content->content;
  $data .= "\r\n--{$boundary}\r\n";
  $data .= 'Content-Disposition: form-data; name="content"';
  $data .= "\r\nContent-Type: application/json\r\n\r\n";
  $data .= @drupal_json_encode($content);
  $data .= "\r\n--{$boundary}--\r\n";

  $headers = array(
    'Content-Type' => 'multipart/form-data; boundary=' . $boundary
  );
  $response = oscaddie_alfresco__alfresco_api__send(OSCADDIE_ALFRESCO_RESOURCE_PATH_FILE, $data, 'POST', $headers, array(), FALSE);

  if (empty($response)) {
    drupal_set_message(t("Alfresco server is not responding. Please contact your network administrator for support."), 'error');
    return FALSE;
  }

  // TODO: Error handling.
  return $response->response;
}
