<?php

/**
 * @file
 * osCaddie Alfresco Node module provies the integration of Alfresco contents
 * and Drupal node contents.
 */

/**
 * Implements hook_node_prepare().
 * Node prepare handler for osCaddie Alfresco Node.
 * @param  Object $node Current node being prepared.
 */
function oscaddie_alfresco_node_node_prepare($node) {
  if ($status = oscaddie_alfresco_model__map_status($node->type)) {
    if ($status->status == OSCADDIE_ALFRESCO_MODEL_MAP_ENABLED
    ||  $status->status == OSCADDIE_ALFRESCO_MODEL_MAP_ALTERED) {

      $node->oscaddie_alfresco['status'] = $status->status;
      $type = $node->type;

      drupal_set_message(
        t(
          '<em>@type</em> is currently being used by osCaddie Alfresco. All changes will be reflected in Alfresco.',
          array('@type' => $type)
        )
      );
    }
  }
}

/**
 * Implements hook_node_load().
 * Node load handler for osCaddie Alfresco Node.
 * @param  array  $nodes An array of nodes being loaded, keyed by nid.
 * @param  array  $types Array containing node types present in $nodes.
 */
function oscaddie_alfresco_node_node_load($nodes, $types) {
  if (count($nodes) == 1) {
    $node = array_pop($nodes);
    if ($status = oscaddie_alfresco_model__map_status($node->type)) {
      if ($status->status == OSCADDIE_ALFRESCO_MODEL_MAP_ENABLED
      ||  $status->status == OSCADDIE_ALFRESCO_MODEL_MAP_ALTERED) {
        $node->oscaddie_alfresco = array(
          'status' => $status->status,
          'uuid'   => oscaddie_alfresco_node__get_uuid($node->nid),
        );
      }
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function oscaddie_alfresco_node_node_insert($node) {
  if (empty($node->type) || empty($node->nid)) {
    return FALSE;
  }
  //  Workflow:
  //  (1) Send content to Alfresco
  //  (2) Save UUID given by Alfresco
  //  (3) Send any file to Alfresco.
  //  (4) Save file UUIDs with FID given by Alfresco
  if (isset($node->oscaddie_alfresco)
  && ($node->oscaddie_alfresco['status'] == OSCADDIE_ALFRESCO_MODEL_MAP_ENABLED
  ||  $node->oscaddie_alfresco['status'] == OSCADDIE_ALFRESCO_MODEL_MAP_ALTERED)) {
    $uuid = NULL;

    //  Extract all useful content
    //  Content Files is an object containing the 'content' and 'files'.
    $content_files = oscaddie_alfresco_node__node_extract($node);

    //  Package the content so it can be sent to Alfresco.
    $contents = array(
      'content_type' => $node->type,
      'content'      => array(
        $content_files->content,
      ),
    );

    $packaged_content = oscaddie_alfresco_node__package_contents($contents);
    $response = oscaddie_alfresco__alfresco_api__content_create($packaged_content);

    if (!empty($response->nids) && (count(get_object_vars($response->nids)) > 0)) {
      //  There should be only one UUID.  Just in case Alfresco returns multiple.
      //  TODO: Send any file to Alfresco.
      foreach ($response->nids as $nid => $uuid_node) {

        if ($nid == $node->nid) {
          drupal_set_message(t("@title has been created on Alfresco.", array('@title' => $node->title)));

          // ATTACH FILE HERE IF THERE IS ONE
          $packaged_files = oscaddie_alfresco_node__attach_file($nid, $uuid_node, $content_files->files, $node->type);

          if (!empty($packaged_files)) {
            $file_response = oscaddie_alfresco__alfresco_api__file_create($packaged_files);

            if ($file_response->status == 'success') {
              drupal_set_message(t('@file uploaded successfully to Alfresco.', array('@file' => $packaged_files->content->file_name)));
            }
            elseif ($file_response->status == 'fail') {
              drupal_set_message(t('Failed to upload file on Alfresco.'));
            }
          }

          oscaddie_alfresco_node_set_uuid($nid, $uuid_node);
        }
        elseif ($response->status == 'fail') {
          //  TODO: IF node failed to create on Alfresco:
          //  (1) Delete node from Drupal.
          //  (2) Prompt to resend.
          drupal_set_message(t("@title failed to create on Alfresco.", array('@title' => $node->title)));

          return FALSE;
        }
      }
    }
  }
}

/**
 * Implements hook_node_update().
 */
function oscaddie_alfresco_node_node_update($node) {
  if (empty($node) || empty($node->nid)) {
    return FALSE;
  }

  //  NEW FEATURE.
  try {
    //  Check if oscaddie_alfresco_model has sync == 0.  If so, ignore this call.
    $sync_type = oscaddie_alfresco_model__return_sync_type($node->type);

    if ($sync_type == 0 || !$sync_type) {
      return FALSE;
    }
  }
  catch (Exception $e) {
    throw new Exception('Failed to return sync type', 0, $e);
  }

  if ($node->oscaddie_alfresco['status'] == OSCADDIE_ALFRESCO_MODEL_MAP_ENABLED
  ||  $nose->oscaddie_alfresco['status'] == OSCADDIE_ALFRESCO_MODEL_MAP_ALTERED) {
    //  We will only Update the content on Alfresco if Alfresco has it.
    //  Extract all useful content.
    //  Content Files is an object containing the 'content' and 'files'.
    try {
      $content_files = oscaddie_alfresco_node__node_extract($node);

      //  Package the content so it will be ready to send to Alfresco.
      $contents = array(
        'content_type' => $node->type,
        'content'      => array(
          $content_files->content,
        ),
      );

      $packaged_content = oscaddie_alfresco_node__package_contents($contents);

      if (!empty($packaged_content)) {
        $response = oscaddie_alfresco__alfresco_api__content_update($packaged_content);

        if (!empty($response->nids)) {
          $packaged_files = oscaddie_alfresco_node__attach_file($nid, $uuid_node, $content_files->files, $node->type);

          if (!empty($packaged_files)) {
            $file_response = oscaddie_alfresco__alfresco_api__file_create($packaged_files);

            if ($fileq_response->status == 'success') {
              drupal_set_message(t('@file uploaded successfully to Alfresco.', array('@file' => $packaged_files->content->file_name)));
            }
            elseif ($file_response->status == 'fail') {
              drupal_set_message(t('@file failed to upload on Alfresco.', array('@file' => $packaged_files->content->file_name)));
            }
          }
        }
      }
    }
    catch (Exception $e) {
      throw new Exception('Failed to package content', 0, $e);
    }
  }
  else {
    drupal_set_message(
      t(
        "Unable to save <em>@title</em> to Alfresco.  Alfresco does not have the content.",
        array('@title' => $node->title)
      ),
      'error',
      FALSE
    );
  }

}

/**
 * Extract all necessary properties from a Node.
 * @param  object $node Reference to a Node Object
 * @return object       An object containing contents and files.
 */
function oscaddie_alfresco_node__node_extract(&$node) {
  $files   = array();
  $content = array();

  $content = array(
     // Internal properties
    'nid'           => $node->nid,
    'author'        => $node->name,
    'comment'       => $node->comment,
    'language'      => $node->language,
    'sticky'        => $node->sticky  ? TRUE : FALSE,
    'promote'       => $node->promote ? TRUE : FALSE,
    'is_published'  => $node->status  ? TRUE : FALSE,
    'publish_date'  => gmdate('m/d/Y', $node->timestamp),
    //  Editable fields
    'title'         => $node->title,
    'body'          => 'This content was created on Drupal.',
    'group'         => 'basic',
  );

  //  Attatch UUID
  if (!empty($node->oscaddie_alfresco['uuid'])) {
    $content['uuid'] = $node->oscaddie_alfresco['uuid'];
  }

  if (isset($node->teaser)) {
    $content['teaser'] = $node->teaser;
  }

  //  TODO:
  //  $content['tag']

  //  Attatch body.
  oscaddie_alfresco_node__field_contents($content, $node);
  $files = oscaddie_alfresco_node__node_extract_files($content);

  if (!$files) {
    $files = array();
  }

  return (object) array(
    'content' => $content,
    'files'   => $files,
  );
}

/**
 * Extract file information from content being sent to Alfresco.
 * @param  array  $content Contains array of FID or single value FID.
 * @return array           Fields that contain files
 */
function oscaddie_alfresco_node__node_extract_files($content) {
  $fields      = array();
  $fields_list = field_info_fields();

  foreach ($fields_list as $name => $info) {
    if (isset($content[$name]) && $info['type'] == 'file') {
      $fields[$name] = $content[$name];
    }
  }
  return sizeof($fields) == 1 ? $fields : FALSE;
}

/**
 * Package content data into a format acceptable by Alfresco.
 * @param  array  $contents An array of content objects and must be in this format:
 *                          array(
 *                            'type_1' => array($content_1a, $content_1b),
 *                            'type_2' => array($content_2a, $content_2b),
 *                          );
 * @return [type]           [description]
 */
function oscaddie_alfresco_node__package_contents($contents) {
  if (empty($contents) || !is_array($contents)) {
    return FALSE;
  }

  $objects = new stdClass();
  $objects->contents = array();
  $objects->contents[] = $contents;

  return !empty($objects->contents) ? $objects : FALSE;

}

/**
 * Implements hook_form_alter().
 * @param  array  $form       Nested array of form elements that compromise
 *                            a form.
 * @param  array  $form_state A keyed array containing the current state of the
 *                            form.
 * @param  string $form_id    String representation of the form itself.
 */
// function oscaddie_alfresco_node_form_alter(&$form, &$form_state, $form_id) {
//   if (array_key_exists('#node', $form)) {
//     $node = $form['#node'];
//     dpm($node, 'oscaddie_alfresco_node_form_alter');
//     if ($node->oscaddie_alfresco['status'] == OSCADDIE_ALFRESCO_MODEL_MAP_ENABLED || $node->oscaddie_alfresco['status'] == OSCADDIE_ALFRESCO_MODEL_MAP_ALTERED) {
//       $form['oscaddie_alfresco'] = array('#tree' => TRUE);

//       $form['oscaddie_alfresco']['status'] = array(
//         '#type' => 'hidden',
//         '#value' => $node->oscaddie_alfresco['status'],
//       );
//       $form['oscaddie_alfresco']['uuid'] = array(
//         '#type' => 'hidden',
//         '#value' => $node->oscaddie_alfresco['uuid'],
//       );
//     }
//   }
// }

/**
 * Cleans the returned Alfresco UUID into trimmed version.
 * @param  string $str_uuid Alfresco UUID.
 * @return string           Cleaned UUID.
 */
function oscaddie_alfresco_node__clean_uuid($str_uuid) {
  if (empty($str_uuid)) {
    return FALSE;
  }

  $uuid = explode('/', $str_uuid);
  $uuid = end($uuid);

  return $uuid;
}

/**
 * Save a UUID with a node ID
 * @param  int    $nid  Node ID for the UUID that is to be saved.
 * @param  string $uuid UUID associated with the Node to be saved.
 */
function oscaddie_alfresco_node_set_uuid($nid, $uuid) {
  if (empty($nid) || !is_numeric($nid) || empty($uuid)) {
    return FALSE;
  }

  try {
    $result = db_select('oscaddie_alfresco_node_uuid', 'i')
            ->fields('i')
            ->condition('i.nid', $nid, '=')
            ->condition('i.uuid', $uuid)
            ->execute()
            ->fetchAssoc();

    if (!empty($result->nid)) {
      //  We will update the UUID.
      db_update('oscaddie_alfresco_node_uuid')
        ->fields('uuid')
        ->values(array(
          'uuid' => $uuid,
        ))
        ->condition('nid', $nid, '=')
        ->execute();
    }
    else {
      //  We will create a new entry of UUID and NID.
      db_insert('oscaddie_alfresco_node_uuid')
        ->fields(array(
          'nid',
          'uuid',
        ))
        ->values(array(
          'nid'  => $nid,
          'uuid' => $uuid,
        ))
        ->execute();
    }
  }
  catch (Exception $e) {
    throw new Exception('DB Query failed.', 0, $e);
  }
}

/**
 * Retrieve the UUID of the files associated with the Node.
 * @param  int    $nid Node ID associated with the UUID we are retrieving.
 * @return String      UUID string if found, otherwise false.
 */
function oscaddie_alfresco_node__get_uuid($nid) {
  if (empty($nid) || !is_numeric($nid)) {
    return FALSE;
  }

  $result = db_select('oscaddie_alfresco_node_uuid', 'i')
          ->fields('i', array('uuid'))
          ->condition('i.nid', $nid, '=')
          ->execute()
          ->fetchAssoc();
  return $result['uuid'];
}

/**
 * Retrieve the Node ID from a provided UUID.
 * @param  string $uuid UUID associated with the node we are retrieving.
 * @return int          Node ID if found, otherwise, false.
 */
function oscaddie_alfresco_node__get_nid_by_uuid($uuid) {
  if (empty($uuid)) {
    return FALSE;
  }

  $result = db_select('oscaddie_alfresco_node_uuid', 'i')
          ->fields('i', array('nid'))
          ->condition('i.uuid', $uuid)
          ->execute()
          ->fetchAssoc();
  return $result;
}

/**
 * Returns field contents based on its cardinality.
 * @param  array  $content Content array that contains everything passed to
 *                         Alfresco.
 * @param  object $node    Node representation of what is being inserted.
 */
function oscaddie_alfresco_node__field_contents(&$content, $node) {
  $field_map = field_info_field_map();
  foreach ($field_map as $field_name => $field_content) {
    if (array_key_exists($field_name, $node)) {
      $type = $field_content['type'];
      $node_fieldname = array_pop($node->$field_name);
      $field_cardinality = sizeof($node_fieldname);

      if ($field_cardinality == 0) {
        unset($content[$field_name]);
      }
      elseif ($field_cardinality == 1) {
        $value = array_pop($node_fieldname);
        if (isset($value['value'])) {
          $content[$field_name] = oscaddie_alfresco__enforce_types($value['value'], $type);
        }
        elseif ($value['fid']) {
          $content[$field_name] = $value['fid'];
        }
      }
      else {
        $field_contents = array();
        foreach ($node_fieldname as $idx => $value) {
          if (isset($value['value'])) {
            $field_contents[] = oscaddie_alfresco__enforce_types($value['value'], $type);
          }
          elseif ($value['fid']) {
            $content[$field_name] = $value['fid'];
          }
        }
        $content[$field_name] = $field_contents;
      }
    }
  }
}

/**
 * Package file into Alfresco acceptable format.
 * @param  String $nid       String representation of Node ID.
 * @param  String $uuid      UUID of specific content node.
 * @param  String $fids      File ID for what is to be uploaded.
 * @param  String $node_type String name of node type.
 * @return Object            Object representation of what is to be sent to Alfresco.
 */
function oscaddie_alfresco_node__attach_file($nid, $uuid, $fids, $node_type) {
  if (!is_numeric($fids)) {
    return FALSE;
  }

  $file = file_load($fid = current($fids));
  $fp = file_create_url($file->uri);

  $file_up = new stdClass();
  $file_up->content = (object) array(
    'content_type' => $node_type,
    'nid'       => $nid,
    'uuid'      => $uuid,
    'fid'       => $fid,
    'content'   => file_get_contents($fp),
    'file_name' => $file->filename,
    'mime_type' => $file->filemime,
  );
  return $file_up;
}
